import Vue from 'vue'
import { Button, Form, 
    FormItem, Input, Message,Container,
    Header,Aside,Main,Menu,Submenu,MenuItem,Breadcrumb,BreadcrumbItem,
    Card,Row,Col,Table,TableColumn,Switch,Tooltip,Pagination,
    Dialog,MessageBox,Tag,Tree,Steps,Step,
    Select,Option,Cascader,Alert,Tabs,TabPane,
    Checkbox,CheckboxGroup,Upload,
    Timeline,
    TimelineItem,
} from 'element-ui'

Vue.use(Timeline)
Vue.use(TimelineItem)

Vue.use(Upload)
Vue.use(Checkbox)
Vue.use(CheckboxGroup)
Vue.use(Step)
Vue.use(Steps)
Vue.use(Tabs)
Vue.use(TabPane)
Vue.use(Alert)
Vue.use(Cascader)
Vue.use(Select)
Vue.use(Option)
Vue.use(Tree)
Vue.use(Tag)
Vue.use(Dialog)
Vue.use(Pagination)
Vue.use(Switch)
Vue.use(Tooltip)
Vue.use(Table)
Vue.use(TableColumn)
Vue.use(Row)
Vue.use(Col)
Vue.use(Card)
Vue.use(Breadcrumb)
Vue.use(BreadcrumbItem)
Vue.use(Menu)
Vue.use(Submenu)
Vue.use(MenuItem)
Vue.use(Header)
Vue.use(Aside)
Vue.use(Main)
Vue.use(Container)
Vue.use(Button)
Vue.use(Form)
Vue.use(FormItem)
Vue.use(Input)
Vue.prototype.$message = Message
Vue.prototype.$confirm = MessageBox.confirm